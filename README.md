# MapdO

MapdO est un **outil en ligne à l’intention des opérateurs** autour des **données et modèles en hydromorphologie**. 
Le projet a débuté en janvier 2022 pour une durée de 3 ans minimum. La production du cahier des charges détaillé est en cours.

<p align="center">
    <img src="./documentation/img/logo_UMR.png" alt="UMR5600 EVS" width="200"/>
    <img src="./documentation/img/logo-ofb.png" alt="Office Français de la Biodiversité" width="250"/>
</p>

## Installation

## Usage

## Contributing

## License

GNU General Public License v3.0