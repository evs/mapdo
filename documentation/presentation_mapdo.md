<div style="text-align: justify">

![](img/logo_ofb_evs_cnrs.png)

# Projet MAPD'O

MAPD’O est un projet visant le développement d'une interface web à l’intention des opérateurs autour des données et modèles en hydromorphologie. C'est un projet réalisé par le laboratoire Environnement Ville Société du CNRS et porté par l'Office Français de la Biodiversité depuis janvier 2023.

L’application repose sur des approches géomatiques d’analyse de la topographie et de l’occupation du sol permettant de produire une carte de continuité latérale des cours d’eau du réseau hydrographique. Différentes métriques morphologiques sont ensuite extraites à intervalle régulier du réseau (largeur, pente, surface drainée, élévation). L’application a pour ambition de fournir des outils d’analyse et d’interprétation de ces métriques afin de faciliter les diagnostics hydromorphologiques des cours d’eau français à l’échelle du bassin versant.

![La continuité latérale de la vallée Drac à Saint-Julien-en-Champsaur](img/drac_continuite.png)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

L'application disposerait de quatre modules : 

- Une interface cartographique permettant de visualiser les métriques du réseau hydrographique mais également des données complémentaires nécessaires à l'interprétation (ROE, BD TOPAGE, géologie, occupation du sol, images aériennes).

- Un module d'analyse longitudinale des métriques.

- Un outil d'analyse régionale qui vise l'identification de tronçons de cours d'eau homogènes du bassin versant.

- Un module d'analyse temporelle représentant les évolutions des surfaces et largeurs du corridor fluvial issus des cartes topographiques du XXème siècle.

![Représentation longitudinale des largeurs du Drac](img/drac_largeur_long.png)

**Contact** : Louis Manière (CNRS UMR 5600, Environnement Ville Société) - louis.maniere@ens-lyon.fr
