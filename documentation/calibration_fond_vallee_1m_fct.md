# Calibration de la détermination des fonds de vallées de la FCT

Fluvial Corridor Toolbox Python
RGE alti 1m

## Test Samuel août 2023 - T001


### Zone de test

Drac et Isère

### Paramètres

``` python
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 20.0, 100.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 20.0, 400.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 20.0, 1500.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 20.0, 2000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 20.0, 2500.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 20.0, 4000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 20.0, 4000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 20.0, 7500.0, 10.5, 2.0)
]
```

### Evaluation (Louis)

#### Fonds de vallée
Bonne continuité générale au niveau des barrages mais avec des exception (l'Eau d'Olle, changer hauteur max)
Beaucoup de bruit dans les tête de bassin (fond de vallée hachuré au niveau sur le Drac par exemple).
La partie plus plane du torrent des Diguières déborde sur la vallée d'à côté.
Présente d'anomalie, des bandes de pixel "valide" de fond de vallée tout en amont de tête de bassin (exemple le Maurian sur l'Isère) ou de résidut de buffer.
Les têtes de bassin sont beaucoup déterminé par le seuil de distance minimale de 20m. c'est souvent une largeur de 20m. Metre un nouveau seuil à 15km2.

#### DGO (SWARTHS_REFAXIS)
Bonne continuité des DGO.
Quelques défaut au niveau de Grenoble où le fond de vallée déborde sur celui d'à côté.
Pas mal d'irrégularité dans la zone de tressage avant la confluence entre l'Isère et le Drac.

### Correction

#### Correction des DGO en amont de la confluence Drac Isère
Attention à la mise à jour du raster des swaths avant leur vectorisation.

Meilleur rendu pour l'amont de la confluence mais dans le secteur amont du Drac problème de avec un tronçon, rupture du medial axis.

#### Correction du bruit dans les têtes de bassin par un nouveau seuil des petits BV
``` python
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 3, 50, 0.5, 12.0),
    ValleyBottomFeatures.ValleyBottomThreshold(15, 20.0, 100.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 20.0, 400.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 20.0, 1500.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 20.0, 2000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 20.0, 2500.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 20.0, 4000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 20.0, 4000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 20.0, 7500.0, 10.5, 2.0)
]
```

## Test Louis - T002

Ajout d'une catégorie à 15km2 de surface de drainage pour éviter le bruit dans les têtes de bassin
### Zone de test

Drac et Isère

### Paramètres

``` python
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 3, 50, 0.5, 12.0),
    ValleyBottomFeatures.ValleyBottomThreshold(15, 20.0, 100.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 20.0, 400.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 20.0, 1500.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 20.0, 2000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 20.0, 2500.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 20.0, 4000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 20.0, 4000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 20.0, 7500.0, 10.5, 2.0)
]
```

### Evaluation (Louis)

Le bruit reste le même dans les résultats, il y a aussi des problème dans les calculs des surfaces drainées au niveau des Swaths.

### Correction

Le réseau hydrographique n'est pas vraiment le bon avec les sections >5m de large correspondant au surface hydrographique.
Changement du réseau.


## Test Louis - T003

Changement du réseau

### Zone de test

Drac et Isère

### Paramètres

Défaut

``` python
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 20.0, 100.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 20.0, 400.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 20.0, 1500.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 20.0, 2000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 20.0, 2500.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 20.0, 4000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 20.0, 4000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 20.0, 7500.0, 10.5, 2.0)
]
```

### Evaluation (Louis)

Plusieurs têtes de bassin supprimés, elle sont surtout dirigé par le distance min et pour certains par le distance max.

### Correction

Test de réduction du distance min et grand élargissemnt du distance max pour uniquement avoir les la hauteur et la pente comme paramètres.

## Test Louis - T004

Suppression distance min et max

### Zone de test

Drac et Isère

### Paramètres

``` python
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 2.0, 10000.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 2.0, 10000.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 2.0, 10000.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 2.0, 10000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 2.0, 10000.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 2.0, 10000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 2.0, 10000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 2.0, 10000.0, 10.5, 2.0)
]
```

### Evaluation (Louis)

Les têtes de bassin bien plus réaliste à la topographie mais davantage de bruit et d'erreur dans les données.
Le bruit est surtout issu du MNT détendancé Nearest Height, déjà buité par shortest height.

### Correction

Test de réduction de variation du bruit dans shortest height

default shortest height params

``` python
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 0.4 # apply jitter on performing shortest path exploration
```

## Test Louis - T004

Suppression jitter dans shortest height pour évaluer son rôle

### Zone de test

Drac et Isère

### Paramètres

shortest height params

sans jitter
``` python
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 0.0 # apply jitter on performing shortest path exploration
```

jitter 0.9
``` python
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 0.9 # apply jitter on performing shortest path exploration
```

jitter 2.0
``` python
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 2 # apply jitter on performing shortest path exploration
```

jitter 10.0
``` python
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 10.0 # apply jitter on performing shortest path exploration
```

### Evaluation (Louis)

Très peu de changements sans jitter.
Petite réduction du bruit avec jitter 0.9 avec un meilleur alignement
Un peu plus d'alignement avvec jitter 2.0 mais les limites sont moins nettes des "transects" 
Beaucoup de bruit interne avec jitter 10.0

### Correction

Pas de correction possible du bruit et des "effets de découpage par transect".
Une solution serait de changer de méthode...
Une amélioration est possible en prenant le RHTS comme référence pour les calculs des shortest height et nearest height

## Test Louis - T005

Calcul des shortest height et nearest height avec le RHTS plutôt qu'avec le réseau BDTOPO de référence

### Zone de test

Drac et Isère

### Paramètres


``` python
# Shortest path exploration
from fct.height import ShortestHeight
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 0.4 # apply jitter on performing shortest path exploration
params.reference = 'streams' #'/data/lmanie01/python-fct/run_drac_isere/fct_workdir/GLOBAL/DEM/RHTS.shp'

# Height above nearest drainage
from fct.height import HeightAboveNearestDrainage
params = HeightAboveNearestDrainage.Parameters()
params.mask_height_max = 100.0 # maximum height defining domain mask
params.buffer_width = 2000 # enlarge domain mask by buffer width expressed in real distance unit (eg. meters)
params.resolution = 1.0 # raster resolution, ie. pixel size, in real distance unit (eg. meters)
params.drainage = 'streams' #'/data/lmanie01/python-fct/run_drac_isere/fct_workdir/GLOBAL/DEM/RHTS.shp'

# paramètre par défaut
params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 20.0, 100.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 20.0, 400.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 20.0, 1500.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 20.0, 2000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 20.0, 2500.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 20.0, 4000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 20.0, 4000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 20.0, 7500.0, 10.5, 2.0)
]

```

### Evaluation (Louis)

Il y a une réduction du bruit visible mais n'évite pas le bruit de forte amplitude, soit les erreurs les plus importantes.
Le réseau hydrographique de référence n'est pas toujours dans la surface du fond de vallée.
Les différences entre le RHTS et le réseau de référence crées des fond de vallée complètement en dehors du cours d'eau.

La continuité au niveau des barrages est mieux assurée mais il peut y avoir des erreurs dans le lac de retenu. Le RHTS est plus simplifié que le réseau BD TOPO, on trouve des "raccourcis" dans les retenus, certainement causé par la topographie plane qui fait que le RHTS va au plus court chemin sans suivre le centre de la morphologie du lac comme la BDTOPO. Le fond de vallée peut ainsi être moins fiable ou erroné avec le RHTS.

### Correction

Pas vraiment d'autre amélioration possible à partir du RHTS, on peut supprimer les contrainte de distance min et max pour avoir le fond de vallée tel qu'il devrait être uniquement avec les critère de hauteur et de pente.

## Test Louis - T006

test fond de vallée depuis le RHTS sans les seuils de distance min et max

### Zone de test

Drac et Isère

### Paramètres

``` python
# Shortest path exploration
from fct.height import ShortestHeight
params = ShortestHeight.Parameters()
params.scale_distance = 1.0 # scale distance output with given scale factor, corresponding to pixel resolution
params.mask_height_max = 100.0 # maximum height defining domain mask
params.height_max = 100.0 # stop at maximum height above reference
params.distance_min = 20.0 # minimum distance before applying stop criteria, expressed in pixels)
params.distance_max = 2000.0 # stop at maximum distance, expressed in pixels
params.jitter = 0.4 # apply jitter on performing shortest path exploration
params.reference = 'streams' #'/data/lmanie01/python-fct/run_drac_isere/fct_workdir/GLOBAL/DEM/RHTS.shp'

# Height above nearest drainage
from fct.height import HeightAboveNearestDrainage
params = HeightAboveNearestDrainage.Parameters()
params.mask_height_max = 100.0 # maximum height defining domain mask
params.buffer_width = 2000 # enlarge domain mask by buffer width expressed in real distance unit (eg. meters)
params.resolution = 1.0 # raster resolution, ie. pixel size, in real distance unit (eg. meters)
params.drainage = 'streams' #'/data/lmanie01/python-fct/run_drac_isere/fct_workdir/GLOBAL/DEM/RHTS.shp'

params.thresholds = [
    # drainage area km², distance min, distance max, max height (depth), max slope (%)
    ValleyBottomFeatures.ValleyBottomThreshold(0, 2.0, 10000.0, 2.0, 10.0),
    ValleyBottomFeatures.ValleyBottomThreshold(30, 2.0, 10000.0, 4.0, 7.0),
    ValleyBottomFeatures.ValleyBottomThreshold(250, 2.0, 10000.0, 5.0, 5.0),
    ValleyBottomFeatures.ValleyBottomThreshold(1000, 2.0, 10000.0, 6.0, 3.5),
    ValleyBottomFeatures.ValleyBottomThreshold(5000, 2.0, 10000.0, 6.5, 3.0),
    ValleyBottomFeatures.ValleyBottomThreshold(11500, 2.0, 10000.0, 7.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(13000, 2.0, 10000.0, 8.5, 2.5),
    ValleyBottomFeatures.ValleyBottomThreshold(45000, 2.0, 10000.0, 10.5, 2.0)
]

```

### Evaluation (Louis) 

Davantage de bruit (sans surprise) mais les fond de vallée sont un peu plus fidèle à la réalité dans les têtes de bassin sans trop de bruit.

### Correction

????